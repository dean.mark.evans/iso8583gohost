package main

import (
	"log"
	"os"
	"net"

	"github.com/moov-io/iso8583"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()

	for {
		// Read the incoming message
		buf := make([]byte, 4096)
		n, err := conn.Read(buf)
		if err != nil {
			log.Printf("Error reading from connection: %v", err)
			return
		}

		go processMessage(buf[:n], conn)
	}
}

func processMessage(buf []byte, conn net.Conn) {
	message := iso8583.NewMessage(iso8583.Spec87)
	err := message.Unpack(buf)
	if err != nil {
		log.Printf("Error unpacking ISO8583 message: %v", err)
		return
	}

	mti, err := message.GetMTI()
	if err != nil {
		log.Printf("Error getting MTI: %v", err)
		return
	}

	if mti == "0800" {
		log.Println("Received logon message")

		// Retrieve Systems Trace Audit Number (STAN)
		stan, err := message.GetField(11).String()
		if err != nil {
			log.Printf("Error getting STAN: %v", err)
			return
		}

		// Retrieve tran time
		trantime, err := message.GetField(7).String()
		if err != nil {
			log.Printf("Error getting trantime: %v", err)
			return
		}

		// Respond with an 0810 message
		response := iso8583.NewMessage(iso8583.Spec87)
		response.MTI("0810") // MTI


		log.Printf("inbound")
		iso8583.Describe(message, os.Stdout)
		
		// Set fields
		response.Field(7, trantime)       // Field 7: Transmission Date & Time
		response.Field(11, stan)         // Field 11: Systems Trace Audit Number
		response.Field(39, "00")         // Field 39: Response Code
		response.Field(40, "300")        // Field 70: Network Management Information Code
		response.Field(70, "301")        // Field 70: Network Management Information Code

		packedResponse, err := response.Pack()
		if err != nil {
			log.Printf("Error packing ISO8583 response message: %v", err)
			return
		}

		_, err = conn.Write(packedResponse)
		if err != nil {
			log.Printf("Error writing response to connection: %v", err)
			return
		}

		log.Printf("outbound")
		iso8583.Describe(response, os.Stdout)

		log.Println("Sent 0810 response message")
	} else if mti == "echo" {

		log.Println("Received echo request message")

		// // Respond with an echo response (0810)
		// response := iso8583.NewMessage(iso8583.Spec87)
		// response.MTI("0810") // MTI

		// // Copy fields from the request message to the response message
		// for i := 2; i <= 128; i++ {
		// 	if field := message.Field(i, iso8583.ASCII); field != nil {
		// 		response.Field(i, field)
		// 	}
		// }

		// packedResponse, err := response.Pack()
		// if err != nil {
		// 	log.Printf("Error packing ISO8583 response message: %v", err)
		// 	return
		// }

		// _, err = conn.Write(packedResponse)
		// if err != nil {
		// 	log.Printf("Error writing response to connection: %v", err)
		// 	return
		// }

		log.Println("Sent echo response message")
	} else {
		log.Printf("Received non-logon message with MTI: %s", mti)
	}
}

func main() {
	listener, err := net.Listen("tcp", ":14001")
	if err != nil {
		log.Fatalf("Error starting TCP server: %v", err)
	}
	defer listener.Close()

	log.Println("Listening for connections on port 14001")

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("Error accepting connection: %v", err)
			continue
		}

		go handleConnection(conn)
	}
}