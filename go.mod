module iso8583host

go 1.21.1

require (
	github.com/moov-io/iso8583 v0.21.2 // indirect
	github.com/yerden/go-util v1.1.4 // indirect
	golang.org/x/text v0.14.0 // indirect
)
